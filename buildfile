#!/bin/bash

# in staging mode :) like ninja

. ${bash_builder_prefix}/library/builder.sh

maintainer Alexey Fedorov "waterlink000+scs@gmail.com"

# config
environ nginx_version 1.2.7
environ nginx_url http://nginx.org/download/nginx-${nginx_version}.tar.gz
environ nginx_folder nginx-${nginx_version}
environ nginx_archive nginx-${nginx_version}.tar.gz
environ nginx_prefix /opt/nginx
environ nginx_config --prefix=${nginx_prefix} --user=www-data --without-mail_pop3_module --without-mail_imap_module --without-mail_smtp_module --with-http_stub_status_module --with-http_ssl_module --with-http_dav_module --with-http_gzip_static_module

# dependencies
package-update
package-install libc6 libpcre3 libpcre3-dev libpcrecpp0 libssl0.9.8 libssl-dev zlib1g zlib1g-dev lsb-base

# installation
download ${nginx_url} ${nginx_archive} ${nginx_folder}
install ${nginx_config}
clean

# nginx config setup
add templates/nginx.conf ${nginx_prefix}/conf/nginx.conf
add templates/fastcgi_params ${nginx_prefix}/conf/fastcgi_params

. library/certs.sh

# nginx config ssl
generate-certificate ${nginx_prefix}/conf/ssl cert
add templates/ssl.conf ${nginx_prefix}/conf/conf.d/ssl.conf

# start script
add templates/nginx.start /etc/init.d/nginx
run chmod +x /etc/init.d/nginx
run update-rc.d nginx defaults
