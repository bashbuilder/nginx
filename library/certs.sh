#!/bin/bash

function generate-certificate {
	oldpwd="$PWD"
	mkdir -p $1
	cd $1
	# prints in case of some questions asked
	printf "\n" | openssl genrsa -out $2.key 2048
	printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n" | openssl req -new -key $2.key -out $2.csr
	printf "\n" | openssl x509 -req -days 365 -in $2.csr -signkey $2.key -out $2.crt
	cd $oldpwd
}
